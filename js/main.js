$().ready(function(){

    $('.btn.btn-success').click(function(){
        document.location = '/?q=addform';
    });
    
    $('#add-form').on('submit', function(e){
        $('.add-error').css({'display': 'none'}).empty();
        
        var url = '?q=create';
        if($('input').is('#post_id')){ url = '?q=update'};
        
        e.preventDefault();
        var $that = $(this),
        formData = new FormData($that.get(0));
        $.post({
          url: url,
          contentType: false, 
          processData: false, 
          data: formData,
          success: function(json){
            var data = jQuery.parseJSON(json);
            
            if(data.error){
                $('.add-error').css({'display': 'block'}).html('Заполните необходмые поля');
            } else {
                location.href = '/?q=index';
            }
          }
        });
      });
    
    $('input').filestyle({
        // button text
        'text' : 'Выбрать изображение',
        // CSS class of button
        'btnClass' : 'btn btn-primary',
        // callback
        'onChange': function () {}

    });
    
    $('.btn.btn-danger').click(function(){
        $.post({
          url: '?q=delete',
          data: 'id='+$(this).attr('id'),
          success: function(json){
            var data = jQuery.parseJSON(json);
            
            if(data.error){
                $('.add-error').css({'display': 'block'}).html('Ошибка удаления');
            } else {
                location.reload();
            } 
          }
        });
    });
    
    
    $('.btn.btn-warning').click(function(){
        
        location.href = '?q=editpost&id='+$(this).attr('id');

    });
    
    $('.btn.btn-secondary').click(function(){
        
        location.href = '?q=index';

    });

        
});