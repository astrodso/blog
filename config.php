<?php
define("SITE_DIR", dirname(__FILE__));
define("DS", DIRECTORY_SEPARATOR);
define('CORE_DIR', SITE_DIR.DS.'classes'.DS.'core'.DS);
define('CONTROLLERS_DIR', SITE_DIR.DS.'controllers'.DS);
define('UPLOADS_DIR', SITE_DIR.DS.'uploads'.DS);

function zend_autoload($Class){
     
     $Class = str_replace('_', DS, $Class);

     if(file_exists(EXT_DIR . $Class . '.php'))
          require_once EXT_DIR . $Class . '.php';
     
}

function auto_loader($class) {
    if(file_exists(CORE_DIR . $class . '.php'))
          require_once CORE_DIR . $class . '.php';
}

function controllers_autoloader($class) {
    if(file_exists(CONTROLLERS_DIR . $class . '.php'))
          require_once CONTROLLERS_DIR . $class . '.php';
}

spl_autoload_register('auto_loader');
spl_autoload_register('controllers_autoloader');
spl_autoload_register('zend_autoload');