-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июл 04 2018 г., 11:12
-- Версия сервера: 5.7.19
-- Версия PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `blog`
--

-- --------------------------------------------------------

--
-- Структура таблицы `images`
--

DROP TABLE IF EXISTS `images`;
CREATE TABLE IF NOT EXISTS `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `postId` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `imageName` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `images`
--

INSERT INTO `images` (`id`, `postId`, `image`, `imageName`) VALUES
(1, 1, '2fd62d44c430b886cd0d602acc3df89f', '300px-NGC_4414_(NASA-med).jpg'),
(2, 2, '26a8d3357af188137ed7afdeea21e6ca', '375px-NGC7293_(2004).jpg'),
(3, 3, '2ea038f07503c3a781466934d3004bed', '375px-Ngc2392.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `content` varchar(1000) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `posts`
--

INSERT INTO `posts` (`id`, `title`, `content`, `date`) VALUES
(2, 'Ð¢ÑƒÐ¼Ð°Ð½Ð½Ð¾ÑÑ‚ÑŒ Ð£Ð»Ð¸Ñ‚ÐºÐ°', 'ÑƒÐ¼Ð°Ð½Ð½Ð¾ÑÑ‚ÑŒ Ð£Ð»Ð¸Ñ‚ÐºÐ° (Helix Nebula, NGC 7293, Ð´Ñ€ÑƒÐ³Ð¸Ðµ Ð¾Ð±Ð¾Ð·Ð½Ð°Ñ‡ÐµÐ½Ð¸Ñ â€” PK 36-57.1, ESO 602-PN22) â€” Ð¿Ð»Ð°Ð½ÐµÑ‚Ð°Ñ€Ð½Ð°Ñ Ñ‚ÑƒÐ¼Ð°Ð½Ð½Ð¾ÑÑ‚ÑŒ Ð² ÑÐ¾Ð·Ð²ÐµÐ·Ð´Ð¸Ð¸ Ð’Ð¾Ð´Ð¾Ð»ÐµÐ¹ Ð½Ð° Ñ€Ð°ÑÑÑ‚Ð¾ÑÐ½Ð¸Ð¸ 650 ÑÐ²ÐµÑ‚Ð¾Ð²Ñ‹Ñ… Ð»ÐµÑ‚ Ð¾Ñ‚ Ð¡Ð¾Ð»Ð½Ñ†Ð°. ÐžÐ´Ð½Ð° Ð¸Ð· ÑÐ°Ð¼Ñ‹Ñ… Ð±Ð»Ð¸Ð·ÐºÐ¸Ñ… Ð¿Ð»Ð°Ð½ÐµÑ‚Ð°Ñ€Ð½Ñ‹Ñ… Ñ‚ÑƒÐ¼Ð°Ð½Ð½Ð¾ÑÑ‚ÐµÐ¹. ÐžÑ‚ÐºÑ€Ñ‹Ñ‚Ð° ÐšÐ°Ñ€Ð»Ð¾Ð¼ Ð›ÑŽÐ´Ð²Ð¸Ð³Ð¾Ð¼ Ð¥Ð°Ñ€Ð´Ð¸Ð½Ð³Ð¾Ð¼ Ð² 1824 Ð³Ð¾Ð´Ñƒ.\r\nÐ­Ñ‚Ð¾Ñ‚ Ð¾Ð±ÑŠÐµÐºÑ‚ Ð²Ñ…Ð¾Ð´Ð¸Ñ‚ Ð² Ñ‡Ð¸ÑÐ»Ð¾ Ð¿ÐµÑ€ÐµÑ‡Ð¸ÑÐ»ÐµÐ½Ð½Ñ‹Ñ… Ð² Ð¿ÐµÑ€Ð²Ð¾Ð¼ Ð¸Ð·Ð´Ð°Ð½Ð¸Ð¸ Â«ÐÐ¾Ð²Ð¾Ð³Ð¾ Ð¾Ð±Ñ‰ÐµÐ³Ð¾ ÐºÐ°Ñ‚Ð°Ð»Ð¾Ð³Ð°Â».', '2018-07-04 11:11:20'),
(3, 'NGC 2392', 'NGC 2392 Ð¸Ð»Ð¸ Ñ‚ÑƒÐ¼Ð°Ð½Ð½Ð¾ÑÑ‚ÑŒ Ð­ÑÐºÐ¸Ð¼Ð¾Ñ â€” Ð¿Ð»Ð°Ð½ÐµÑ‚Ð°Ñ€Ð½Ð°Ñ Ñ‚ÑƒÐ¼Ð°Ð½Ð½Ð¾ÑÑ‚ÑŒ Ð² ÑÐ¾Ð·Ð²ÐµÐ·Ð´Ð¸Ð¸ Ð‘Ð»Ð¸Ð·Ð½ÐµÑ†Ñ‹. Ð¢ÑƒÐ¼Ð°Ð½Ð½Ð¾ÑÑ‚ÑŒ Ð±Ñ‹Ð»Ð° Ð¾Ñ‚ÐºÑ€Ñ‹Ñ‚Ð° Ð£Ð¸Ð»ÑŒÑÐ¼Ð¾Ð¼ Ð“ÐµÑ€ÑˆÐµÐ»ÐµÐ¼ Ð² 1787 Ð³Ð¾Ð´Ñƒ. Ð¡Ð²Ð¾Ñ‘ Ð½Ð°Ð·Ð²Ð°Ð½Ð¸Ðµ Ð¾Ð½Ð° Ð¿Ð¾Ð»ÑƒÑ‡Ð¸Ð»Ð° Ð¸Ð·-Ð·Ð° Ñ‚Ð¾Ð³Ð¾, Ñ‡Ñ‚Ð¾ Ñ Ð—ÐµÐ¼Ð»Ð¸ Ð¾Ð½Ð° Ð¿Ð¾Ñ…Ð¾Ð¶Ð° Ð½Ð° Ð³Ð¾Ð»Ð¾Ð²Ñƒ Ñ‡ÐµÐ»Ð¾Ð²ÐµÐºÐ° Ð² ÐºÐ°Ð¿ÑŽÑˆÐ¾Ð½Ðµ. Ð Ð°ÑÑÑ‚Ð¾ÑÐ½Ð¸Ðµ Ð¾Ñ‚ Ð¡Ð¾Ð»Ð½ÐµÑ‡Ð½Ð¾Ð¹ ÑÐ¸ÑÑ‚ÐµÐ¼Ñ‹ Ð´Ð¾ Ñ‚ÑƒÐ¼Ð°Ð½Ð½Ð¾ÑÑ‚Ð¸ ÑÐ¾ÑÑ‚Ð°Ð²Ð»ÑÐµÑ‚ 3000 ÑÐ²ÐµÑ‚Ð¾Ð²Ñ‹Ñ… Ð»ÐµÑ‚, Ñ€Ð°Ð·Ð¼ÐµÑ€ ÑÐ°Ð¼Ð¾Ð¹ Ñ‚ÑƒÐ¼Ð°Ð½Ð½Ð¾ÑÑ‚Ð¸ Ñ‚Ñ€ÐµÑ‚ÑŒ ÑÐ²ÐµÑ‚Ð¾Ð²Ð¾Ð³Ð¾ Ð³Ð¾Ð´Ð°, Ð¾Ð´Ð½Ð°ÐºÐ¾ Ð½ÐµÐºÐ¾Ñ‚Ð¾Ñ€Ñ‹Ðµ Ð³Ð°Ð·Ð¾Ð²Ñ‹Ðµ Ð²Ð¾Ð»Ð¾ÐºÐ½Ð° Ð¿Ñ€Ð¾ÑÑ‚Ð¸Ñ€Ð°ÑŽÑ‚ÑÑ Ð½Ð° ÑÐ²ÐµÑ‚Ð¾Ð²Ð¾Ð¹ Ð³Ð¾Ð´.', '2018-07-04 11:11:58');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
