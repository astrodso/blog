<?php

/**
 * Базовый контроллер
 * @author Максим Топузов
 * @version 1.0
 */
class Controller {

    /**
     * Вызывает метод
     * @param string $action название метода
     * @access public
     */
    public function action($action) {

        $action = strtolower($action);

        if (method_exists($this, $action))
            $this->$action();
    }

    /**
     * Выводит въюху
     * @param string $view
     * @param array $data
     * @access protected
     */
    protected function view($view, $data = array()) {

        $head = SITE_DIR . DS . "views" . DS . "head.php";

        $footer = SITE_DIR . DS . "views" . DS . "footer.php";

        $content = SITE_DIR . DS . "views" . DS . "$view.php";

        ob_start('ob_gzhandler');

        if (file_exists($head) && file_exists($content) && file_exists($footer)) {

            foreach ($data as $key => $value) {
                $$key = $value;
            }
            require_once $head;
            require_once $content;
            require_once $footer;
        }

        ob_end_flush();
    }

    /**
     * Редирект
     * @param string $url
     * @access protected
     */
    protected function redirect($url) {
        header("Location: $url");
    }

}

?>
