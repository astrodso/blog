<?php

/**
 * Класс для работы с базой данных
 *
 * @author Максим Топузов
 * @version 1.0
 */
class DB {

    /**
     * Переменная содержит PDO объект
     * @var object 
     * @access protected
     */
    protected static $DB = null;

    /**
     * кофигурация
     * @var array
     * @access private
     */
    private static $config;

    private function __clone() {
        
    }

    /**
     * Возвращает объект PDO
     * @return object
     * @access public
     */
    public static function getDB() {
        // проверяем актуальность экземпляра
        if (null === self::$DB) {
            //загружаем конфиг
            self::configLoad();
            // создаем новый экземпляр
            self::$DB = new PDO("mysql:host=" .
                    self::$config['db']['dbHost'] .
                    ";dbname=" . self::$config['db']['dbName'], self::$config['db']['dbUser'], self::$config['db']['dbPassword']
            );
        }
        // возвращаем созданный или существующий экземпляр
        return self::$DB;
    }

    public static function insertId() {
        return self::getDB()->lastInsertId();
    }

    /**
     * Вставляет данные в таблицу
     * Массив вида ключ => значение
     * @param type $table string
     * @param type $rows array
     * @access public
     */
    public static function insert($table, $rows = array()) {

        $insert_keys = implode('`, `', array_keys($rows));

        $insert_values = implode("', '", $rows);

        self::getDB()->query("INSERT INTO `" . $table . "` (`$insert_keys`) VALUES ('$insert_values')");

        $error = self::getDB()->errorInfo();

        if (isset($error[2]))
            die($error[2]);

        return true;
    }

    /**
     * Выборка из базы
     * @param string $table
     * @param array $rows
     * @param string $where
     * @param array $limit
     * @param array $order
     * @return array
     * @throws Exception
     * @access public
     */
    public static function select($table, $rows = '*', $join = '', $where = '1', $limit = array(), $order = '') {

        $data = array();

        if (is_array($rows)) {
            $rows = implode(", ", $rows);
        } elseif ($rows != '*') {
            throw new Exception('Требуется массив !');
        }

        if (is_array($order)) {
            $orderBy = 'ORDER BY ';

            foreach ($order as $key => $val) {
                if (next($order)) {
                    $orderBy .= '`' . $key . '` ' . strtoupper($val) . ', ';
                } else {
                    $orderBy .= '`' . $key . '` ' . strtoupper($val);
                }
            }

            $order = $orderBy;
        }

        if (!empty($limit)) {
            if (isset($limit[1]))
                $limit = 'LIMIT ' . $limit[0] . ', ' . $limit[1];
            else
                $limit = 'LIMIT ' . $limit[0] . ', 9999999999';
        } else {
            $limit = '';
        }

//          echo "SELECT $rows FROM `" . $table . "` $join WHERE $where $order $limit";die;

        $result = self::getDB()->query("SELECT $rows FROM " . $table . " $join WHERE $where $order $limit");

        $error = self::getDB()->errorInfo();

        if (isset($error[2]))
            die($error[2]);

        while ($array = $result->fetch()) {
            $data[] = $array;
        }

        return $data;
    }

    /**
     * Elakztn данные из таблицы
     * @param string $table
     * @param string $where
     * @return boolean
     * @access public
     */
    public static function delete($table, $where = '1') {

        self::getDB()->query("DELETE FROM `" . $table . "` WHERE $where");

        $error = self::getDB()->errorInfo();

        if (isset($error[2]))
            return false;

        return true;
    }

    /**
     * Обновляет данные в таблице
     * @param string $table
     * @param array $data
     * @param string $where
     * @return boolean
     * @access public
     */
    public static function update($table, $data, $where = 1) {

        $query = "UPDATE `$table` SET ";

        foreach ($data as $key => $val) {

            if (next($data))
                $query .= "`$key` = '$val', ";
            else
                $query .= "`$key` = '$val'";
        }

        $query .= " WHERE $where";

        self::getDB()->query($query);

        $error = self::getDB()->errorInfo();

        if (isset($error[2]))
            die($error[2]);

        return TRUE;
    }

    /**
     * Блокировка таблиц
     * @param array $tables array( table => MODE)
     * @return void
     * @access public
     */
    public static function LockTables($tables) {

        $q = 'LOCK TABLES ';

        if (is_array($tables)) {

            foreach ($tables as $key => $mode) {

                if (next($tables))
                    $q .= strtolower($key) . ' ' . strtoupper($mode) . ', ';
                else
                    $q .= strtolower($key) . ' ' . strtoupper($mode);
            }
        }

        self::getDB()->query($q);
    }

    /**
     * Разблокировка таблиц
     * @return void
     * @access public
     */
    public static function UnlockTables() {

        self::getDB()->query('UNLOCK TABLES');
    }

    /**
     * Загружает конфигурационный массив
     * @return void
     * @access private
     */
    private static function configLoad() {
        if (!is_array(self::$config))
            self::$config = include SITE_DIR . DS . "db_config.php";;
    }

}

?>
