
<form id="add-form" enctype="multipart/form-data">
  <div class="form-group">
    <label for="title">Заголовок</label>
    <input type="text" class="form-control" id="title" name="title" value="<?= $post['title'] ?? ''?>" placeholder="Введите заголовок">
  </div>
  <div class="form-group">
    <label for="content">Контент</label>
    <textarea class="form-control" id="content" name="content"><?= $post['content'] ?? ''?></textarea>
  </div>
  <div class="form-group">
    <?php if(isset($post['image'])):?>
      <img width="150" src="/uploads/<?= $post['image'];?>" />
    <?php endif;?>
    <label class="custom-file">
        <input type="file" id="image" name="image" class="custom-file-input" accept="image/*">
        <span class="custom-file-control"></span>
    </label>
  </div>
    <?php if(isset($post['id'])):?>
    <input type="hidden" id="post_id" name="post_id" value="<?= $post['id']?>" />
        <button type="submit" class="btn btn-primary">Обновить</button>
    <?php else:?>
        <button type="submit" class="btn btn-primary">Добавить</button>
    <?php endif;?>
  
  <button type="submit" id="add-post" class="btn btn-secondary">Отмена</button>
</form>