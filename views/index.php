<button type="button" class="btn btn-success">Добавить запись</button>

<table class="table table-striped"> 
    <thead class="thead-inverse">
        <tr>
          <th>Изображение</th>
          <th>Заголовок</th>
          <th>Контент</th>
          <th>Дата</th>
          <th></th>
          <th></th>
        </tr>
    </thead>
    <?php foreach ($posts as $post):?>
    <tr>
        <td><img width="150" src="/uploads/<?= $post['image'];?>" /></td>
        <td><?= $post['title'];?></td>
        <td><?= $post['content'];?></td>
        <td><?= $post['date'];?></td>
        <td><button type="button" id="id_<?=$post['id']?>" class="btn btn-warning">Редактировать</button></td>
        <td><button type="button" id="id_<?=$post['id']?>" class="btn btn-danger">Удалить</button></td>
    </tr>
    <?php endforeach;?>
</table>

