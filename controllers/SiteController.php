<?php

class SiteController extends Controller {

    public function index() {
        $posts = DB::select('posts',
                        ['images.image', 'posts.id', 'posts.title', 'posts.content', 'posts.date'],
                        'LEFT JOIN images ON posts.id = images.postId');

        $this->view('index', ['posts' => $posts]);
    }

    public function addform() {
		
        $this->view('form');
		
    }

    public function editpost() {
		
        $id = (int) str_replace('id_', '', $_GET['id']) ?? null;

        if ($id == null) {
            $this->redirect('/?q=index');
        }

        $post = DB::select('posts',
                        ['images.image', 'posts.id', 'posts.title', 'posts.content', 'posts.date'],
                        'LEFT JOIN images ON posts.id = images.postId', "posts.id=$id");

        $this->view('form', ['post' => $post[0]]);
    }

    public function update() {

        $id = $_POST['post_id'];
        $title = $_POST['title'];
        $content = $_POST['content'];
        $imgName = $_FILES['image']['name'];
        $imageHash = md5(basename($imgName) . time());
        $image = UPLOADS_DIR . $imageHash;
        $error = ['error' => false];

        if (empty($title) || empty($content) || empty($imgName)) {
            $error['error'] = true;
        } else {

            DB::update('posts', [
                'title' => $title,
                'content' => $content,
                'date' => date('Y-m-d H:i:s')
                    ],
                    "id=$id");

            DB::update('images', [
                'postId' => $id,
                'image' => $imageHash,
                'imageName' => $imgName,
            ]);
            move_uploaded_file($_FILES['image']['tmp_name'], $image);
        }

        echo json_encode($error);
    }

    public function create() {
    
        $title = $_POST['title'];
        $content = $_POST['content'];
        $imgName = $_FILES['image']['name'];
        $imageHash = md5(basename($imgName) . time());
        $image = UPLOADS_DIR . $imageHash;
        $error = ['error' => false];

        if (empty($title) || empty($content) || empty($imgName)) {
            $error['error'] = true;
        } else {

            DB::insert('posts', [
                'title' => $title,
                'content' => $content,
                'date' => date('Y-m-d H:i:s')
            ]);

            DB::insert('images', [
                'postId' => DB::insertId(),
                'image' => $imageHash,
                'imageName' => $imgName,
            ]);
            move_uploaded_file($_FILES['image']['tmp_name'], $image);
        }

        echo json_encode($error);
    }

    public function delete() {
        $error = ['error' => false];

        $id = (int) str_replace('id_', '', $_POST['id']);
        if (!DB::delete('posts', 'id=' . $id)) {
            $error['error'] = true;
        }

        echo json_encode($error);
    }

}
